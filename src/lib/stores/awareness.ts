import { readable } from "svelte/store"
import type { Readable } from "svelte/store"
import type { Awareness } from "y-protocols/awareness"

export type AwarenessStates<T> = Map<number, T>

export function readableAwarenessStates<T>(awareness: Awareness): Readable<AwarenessStates<T>> {
	function getStates(): AwarenessStates<T> {
		return awareness.getStates() as AwarenessStates<T>
	}

	return readable(getStates(), (set) => {
		awareness.on("change", () => {
			set(getStates())
		})
	})
}
