import QRCode from "qrcode"

export interface QRCodeActionParams {
	text: string
}

export interface QRCodeActionOutput {
	update(params: QRCodeActionParams): void
}

export function qrcode(node: HTMLCanvasElement, params: QRCodeActionParams): QRCodeActionOutput {
	function create(params: QRCodeActionParams) {
		const { text } = params
		QRCode.toCanvas(node, text, function (error: Error) {
			if (error) {
				throw error
			}
		})
	}
	create(params)
	return {
		update(params: QRCodeActionParams) {
			create(params)
		},
	}
}
