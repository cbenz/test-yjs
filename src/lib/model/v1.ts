import type { Entity } from "./repository"

export interface PersonProperties {
	name: string
	size: number
	friends: string[]
}

export type Person = Entity & PersonProperties

export function createPersonProperties(): PersonProperties {
	return { name: "", size: 0, friends: [] }
}
