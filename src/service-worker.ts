import { build, files, timestamp } from "$service-worker"

import { setCacheNameDetails } from "workbox-core"
import { createHandlerBoundToURL, precache, precacheAndRoute } from "workbox-precaching"
import { NavigationRoute, registerRoute } from "workbox-routing"

setCacheNameDetails({ suffix: timestamp.toString() })

precache(["/"])
precacheAndRoute([...build, ...files])

const handler = createHandlerBoundToURL("/")
const navigationRoute = new NavigationRoute(handler)
registerRoute(navigationRoute)
