import * as localStorageStore from "svelte-local-storage-store"
import { derived, get } from "svelte/store"
import type { Readable } from "svelte/store"

export interface SetStore<T> extends Readable<Set<T>> {
	add(value: T): void
}

function createPersistedSetStore<T>(
	localStorageKey: string,
	initial: Set<T> = new Set(),
): SetStore<T> {
	const arrayStore = localStorageStore.writable<T[]>(localStorageKey, [...initial])
	const setStore = derived(arrayStore, ($arrayStore) => new Set($arrayStore))
	return {
		subscribe: setStore.subscribe,
		add(value: T): void {
			if (get(setStore).has(value)) {
				return
			}
			arrayStore.update(($arrayStore) => [...$arrayStore, value])
		},
	}
}

export const visitedRoomNames = createPersistedSetStore<string>(
	"visitedRoomNames",
	new Set(["demo"]),
)
