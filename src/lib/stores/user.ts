import { adjectives, colors, animals, uniqueNamesGenerator } from "unique-names-generator"

import * as localStorageStore from "svelte-local-storage-store"

function generateUserName(): string {
	return uniqueNamesGenerator({ dictionaries: [adjectives, colors, animals] })
}

export const userName = localStorageStore.writable("userName", generateUserName())
