import type { Readable } from "svelte/store"
import type { Awareness } from "y-protocols/awareness"
import { AwarenessStates, readableAwarenessStates } from "$lib/stores/awareness"
import type { EntityId } from "$lib/model/repository"

export interface CursorInfo {
	personId: EntityId
	fieldName: string
}

export interface UserInfo {
	name: string
}

export interface AwarenessInfo {
	cursor?: CursorInfo
	user: UserInfo
}

export type SetCursorInfo = (cursorInfo: CursorInfo) => void
export type SetUserInfo = (userInfo: UserInfo) => void

export interface UseAwarenessOutput<T> {
	setCursorInfo: SetCursorInfo
	setUserInfo: SetUserInfo
	states: Readable<AwarenessStates<T>>
}

export function useAwareness<T>(awareness: Awareness): UseAwarenessOutput<T> {
	const states = readableAwarenessStates<T>(awareness)

	function setCursorInfo(cursorInfo: CursorInfo): void {
		awareness.setLocalStateField("cursor", cursorInfo)
	}

	function setUserInfo(userInfo: UserInfo): void {
		awareness.setLocalStateField("user", userInfo)
	}

	return { setCursorInfo, setUserInfo, states }
}
