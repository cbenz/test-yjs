import { v4 as uuid } from "@lukeed/uuid"
import { derived } from "svelte/store"
import type { Readable } from "svelte/store"
import { readableMap } from "svelt-yjs"
import type { SetOptional } from "type-fest"
import type * as Y from "yjs"
import type { Entity, EntityHistoryEntry, EntityId, Repository } from "$lib/model/repository"
import { byProp } from "$lib/utils"

interface YjsRepositoryCreateOptions {
	ydoc: Y.Doc
}

export class YjsRepository<T extends Entity> implements Repository<T> {
	private constructor(private entityName: string, private ydoc: Y.Doc) {}

	static create<T extends Entity>(
		entityName: string,
		options: YjsRepositoryCreateOptions,
	): YjsRepository<T> {
		const { ydoc } = options
		return new YjsRepository<T>(entityName, ydoc)
	}

	delete(entityId: EntityId): void {
		this.getEntityYMap().delete(entityId)
	}

	historyStore(): Readable<Map<EntityId, EntityHistoryEntry<T>[]>> {
		return readableMap(this.getEntityHistoryYMap())
	}

	list(): T[] {
		return Array.from(this.getEntityYMap().values()).sort(byProp("id"))
	}

	listStore(): Readable<T[]> {
		return derived(readableMap(this.getEntityYMap()), (map: Map<EntityId, T>) =>
			Array.from(map.values()).sort(byProp("id")),
		)
	}

	save(entity: SetOptional<T, "id">): void {
		const entityId = entity.id ?? uuid()
		this.ydoc.transact(() => {
			this.getEntityYMap().set(entityId, { ...entity, id: entityId } as T)
			const yhistory = this.getEntityHistoryYMap()
			const entities = yhistory.get(entityId) ?? []
			yhistory.set(entityId, [
				...entities,
				{
					entity: { ...entity, id: entityId } as T,
					updatedAt: Date.now(),
				},
			])
		})
	}

	private getEntityHistoryYMap(): Y.Map<EntityHistoryEntry<T>[]> {
		return this.ydoc.getMap(`${this.entityName}:history`)
	}

	private getEntityYMap(): Y.Map<T> {
		return this.ydoc.getMap(this.entityName)
	}
}
