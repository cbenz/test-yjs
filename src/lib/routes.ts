class RouteBuilder {
	#basePath: string

	constructor(basePath = "/") {
		this.#basePath = trimEndSlash(basePath)
	}

	room(roomName: string): string {
		return `${this.#basePath}/room/${roomName}`
	}

	root(): string {
		return `${this.#basePath}/`
	}
}

function trimEndSlash(s: string): string {
	return s.replace(/\/$/, "")
}

export default new RouteBuilder()
