export function byProp<T, K extends keyof T>(propName: K) {
	return function (a: T, b: T): number {
		return a[propName] < b[propName] ? -1 : a[propName] > b[propName] ? 1 : 0
	}
}
