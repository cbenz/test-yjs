import { nanoid } from "nanoid"

const roomNameLength = 32

export function generateRoomName(): string {
	// TODO use cryptographically secure string
	return nanoid(roomNameLength)
}

export function isValidRoomName(roomName: string): boolean {
	return roomName.trim().length > 0
}
