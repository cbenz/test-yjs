import type * as Y from "yjs"
import { IndexeddbPersistence } from "y-indexeddb"

export interface UseYDocInput {
	initModel?: (doc: Y.Doc) => void
	roomName: string
	ydoc: Y.Doc
}

export function useYDoc(input: UseYDocInput): Promise<void> {
	const { initModel, roomName, ydoc } = input
	const indexeddbPersistence = new IndexeddbPersistence(roomName, ydoc)
	return indexeddbPersistence.whenSynced.then(() => {
		if (initModel !== undefined) {
			initModel(ydoc)
		}
	})
}
