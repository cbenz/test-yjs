import type * as Y from "yjs"

interface Meta {
	modelVersion: number
}

export function initModel(ydoc: Y.Doc): void {
	function throwError(message: string): never {
		ydoc.destroy()
		throw new Error(message)
	}

	const metaMap: Y.Map<Meta> = ydoc.getMap("meta")
	const meta = metaMap.get("meta") ?? { modelVersion: 1 }
	const { modelVersion } = meta

	console.log(`initModel: found model version ${modelVersion}`)

	// throwError("model version error")

	if (modelVersion > 1) {
		throwError("model version too recent, please update app")
	}
}
