import type { Readable } from "svelte/store"
import type { SetOptional } from "type-fest"

export type EntityId = string

export interface Entity {
	id: EntityId
}

export interface EntityHistoryEntry<T extends Entity> {
	entity: T
	updatedAt: number
}

export interface Repository<T extends Entity> {
	delete(entityId: EntityId): void
	historyStore(): Readable<Map<EntityId, EntityHistoryEntry<T>[]>>
	list(): T[]
	listStore(): Readable<T[]>
	save(entity: SetOptional<T, "id">): void
}
